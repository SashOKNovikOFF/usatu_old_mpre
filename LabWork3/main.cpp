#define _SCL_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES

#include "Data.h"

#include <fstream>
#include <vector>
#include <iostream>
#include <random>
#include <iomanip>

int length = 500;                      // ����� ��������� �������
std::vector<Data> data(length);         // ��������� ������
std::vector<double> medianDiff(length); // ���������� ������� �� �������

std::string inputFile = "Searcher 1.txt"; // ������� ����
std::string outputFile = "Result.txt";        // �������� ����

// ������������ ���������
std::vector<double> studentCoeff5(length + 1);   // 5% ����������
std::vector<double> studentCoeff001(length + 1); // 0.01% ����������

double arythMean;                       // ������� ��������
std::vector<double> difference(length); // ����������
double variance;                        // ���������
double SADeviation;                     // �������������������� ����������
double SDeviation;                      // ����������� ����������
double m1;                              // ������ 1 �������
double m3;                              // ������ 3 �������
double m4;                              // ������ 4 �������
double covariance;                      // ��������

const static double errorsCoeff = 3.0;  // �����������, ���������� �� ����� ������ ������������
const static double tableHI = 10.645; // ��������� �������� ��-������������� ��� n = 6
const static double tableD_ = 0.121;  // ��������� �������� �������� �������� �����������-�������� ��� n = 100

double g1;   // ���������� ����������
double g2;   // ���������� ��������
double G1;   // ����������� ������ ���������� ����������
double G2;   // ����������� ������ ���������� ��������
double S_G1; // ������������������ ���������� G1
double S_G2; // ������������������ ���������� G1

int k = 9;   // ����� �������
int minIndex; // ������ ������������ �������� �������
int maxIndex; // ������ ������������� �������� �������
double HI;    // ����������� ��-�������

double tao;
double tao5;
double tao001;

std::vector<double> xMean(k);      // �������� ������� x
double step;                       // ������ ������ ������
std::vector<double> border(k + 1, 0); // ������� ����� ��������
std::vector<int> B(k, 0);             // ������� ��� ���� ������� B
std::vector<double> BX(k), BX2(k);    // Bx � Bx^2	
double sumBX = 0;                  // ����� ���� Bx
double sumBX2 = 0;                 // ����� Bx � ��������
double x_, S_;                     // x � ������ � S � ������
double k_;                         // ����������� k'
std::vector<double> z(k, 0);             // ������ z
std::vector<double> FZ(k);               // ������ F(z)
std::vector<double> E(k), BE(k), BE2(k); // ������ E, BE, BE2
std::vector<double> FB(k), FE(k), FBFE(k); // ������������ F_B, F_E, F_B - F_E
double D_;                                 // �������� �������� �����������-��������


void inputData();             // ������ ������ � ���������
void getSampleCoeff();        // ��������� ���������� ��������������
bool screenErrors();          // ��������� ������� �� ������ ������
bool checkNormalDist();       // ��������� �������� ������������ �������������
void outputData();            // �������� ������ � ����
double getPDFValue(double z); // �������� �������� ��������� ����������� ����������� �������������
void findMinMaxElements();    // ����� ������ ������������� � ������������ �������� �������
bool checkHiCoeff();          // ��������� ������������ ������������� ��� ������ ��-��������

void inputData(std::string& inputFile)
{
    // �������� ������ ��� ����� ������ �� �����
    std::ifstream file(inputFile);

    // ���� ��������� ������ �� �����
    double temp;
    for (int number = 0; number < length; number++)
    {
        //file >> temp;
        data[number].time = 0;//temp;

		file >> temp;
        data[number].stress = temp;
    };

    file.close();

    // �������� ������ ��� ����� ������ �� �����
    std::ifstream student("StudentCoeff.txt");

    // ���� ��������� ������ �� �����
    temp = 0.0;
    for (int number = 1; number <= length; number++)
    {
        student >> temp;
        studentCoeff5[number] = temp;
        student >> temp;
        studentCoeff001[number] = temp;
        student >> temp;
    };

    student.close();
};
void getSampleCoeff()
{
    arythMean = 0.; // ������� ��������
    for (int i = 0; i < length; i++)
        arythMean += data[i].stress;
    arythMean /= length;

    // ����������
    for (int i = 0; i < length; i++)
        difference[i] = data[i].stress - arythMean;

    variance = 0.;    // ���������
    SADeviation = 0.; // �������������������� ����������
    SDeviation = 0.;  // ����������� ����������
    for (int i = 0; i < length; i++)
        variance += difference[i] * difference[i];
    SADeviation = sqrt(variance / length);
    SDeviation = sqrt(variance / (length - 1));
    variance /= length;

    m1 = 0.; // ������ 1 �������
    for (int i = 0; i < length; i++)
        m1 += difference[i];
    m1 /= length;

    m3 = 0.; // ������ 3 �������
    for (int i = 0; i < length; i++)
        m3 += pow(difference[i], 3.);
    m3 /= length;

    m4 = 0.; // ������ 4 �������
    for (int i = 0; i < length; i++)
        m4 += pow(difference[i], 4.);
    m4 /= length;

    covariance = SDeviation / arythMean; // ��������
};
bool screenErrors()
{
    int maxDiffIndex = 0; // ���������� ����������
    for (int i = 0; i < length; i++)
        if (std::abs(difference[maxDiffIndex]) < std::abs(difference[i]))
            maxDiffIndex = i;

    tao = std::abs(difference[maxDiffIndex]) / SDeviation;
    tao5 = studentCoeff5[length - 2] * sqrt(length - 1) / sqrt(length - 2 + pow(studentCoeff5[length - 2], 2.));
    tao001 = studentCoeff001[length - 2] * sqrt(length - 1) / sqrt(length - 2 + pow(studentCoeff001[length - 2], 2.));

    if (tao / errorsCoeff > tao001)
    {
        data.erase(data.begin() + maxDiffIndex);
        length--;
        return true;
    }
    else
        return false;
};
bool checkNormalDist()
{
    g1 = m3 / pow(variance, 1.5);
    g2 = m4 / pow(variance, 2.) - 3.;

    G1 = sqrt(length - 1.) / (length - 2.) * g1;                                    // ���������� ����������
    G2 = (length - 1.) / (length - 2.) / (length - 3.) * ((length + 1.) * g2 + 6.); // ���������� ��������

    S_G1 = sqrt(6. * length * (length - 1.) / (length - 2.) / (length + 1.) / (length + 3.));
    S_G2 = sqrt(24. * length * pow(length - 1, 2.) / (length - 3.) / (length - 2.) / (length + 3.) / (length + 5.));

    if (!((std::abs(G1) <= 3. * S_G1) && (std::abs(G2) <= 3. * S_G2)))
        return false;

    return true;
};
void outputData(std::string& outputFile)
{
    // �������� ������ ��� ����� ������ �� �����
    std::ofstream output(outputFile);

    // ����� ���������� ������������� � ������
    output << "x = " << arythMean << std::endl;
    output << "m1 = " << m1 << std::endl;
    output << "m2 = " << variance << std::endl;
    output << "m3 = " << m3 << std::endl;
    output << "m4 = " << m4 << std::endl;
    output << "S_ = " << SADeviation << std::endl;
    output << "S = " << SDeviation << std::endl;
    output << "v = " << covariance << std::endl;

    // ����� ������������� tao
    output << "tao = " << tao << std::endl;
    output << "tao5 = " << tao5 << std::endl;
    output << "tao001 = " << tao001 << std::endl;

    // ����� ������������� ���������� � ��������
    output << "g1 = " << g1 << std::endl;
    output << "g2 = " << g2 << std::endl;
    output << "G1 = " << G1 << std::endl;
    output << "G2 = " << G2 << std::endl;
    output << "S_G1 = " << S_G1 << std::endl;
    output << "S_G2 = " << S_G2 << std::endl;
    output << "3 * S_G1 = " << 3 * S_G1 << std::endl;
    output << "3 * S_G2 = " << 3 * S_G2 << std::endl;

    // ����� ������������� ��� �������� �������� �������
    output << "k = " << k << std::endl;
    output << "x_ = " << x_ << std::endl;
    output << "S_ = " << S_ << std::endl;
    output << "k' = " << k_ << std::endl;
    output << "HI = " << HI << std::endl;
    output << "TABLE_HI = " << tableHI << std::endl;

    output << "B: " << std::endl;
    for (int i = 0; i < k; i++)
        output << B[i] << std::endl;
    output << "-----" << std::endl;

    output << "x: " << std::endl;
    for (int i = 0; i < k; i++)
        output << xMean[i] << std::endl;
    output << "-----" << std::endl;

    output << "Bx: " << std::endl;
    for (int i = 0; i < k; i++)
        output << BX[i] << std::endl;
    output << "-----" << std::endl;

    output << "Bx2: " << std::endl;
    for (int i = 0; i < k; i++)
        output << BX2[i] << std::endl;
    output << "-----" << std::endl;

    output << "z: " << std::endl;
    for (int i = 0; i < k; i++)
        output << z[i] << std::endl;
    output << "-----" << std::endl;

    output << "f(z): " << std::endl;
    for (int i = 0; i < k; i++)
        output << FZ[i] << std::endl;
    output << "-----" << std::endl;

    output << "E: " << std::endl;
    for (int i = 0; i < k; i++)
        output << E[i] << std::endl;
    output << "-----" << std::endl;

    output << "B - E: " << std::endl;
    for (int i = 0; i < k; i++)
        output << BE[i] << std::endl;
    output << "-----" << std::endl;

    output << "(B - E)2: " << std::endl;
    for (int i = 0; i < k; i++)
        output << BE2[i] << std::endl;
    output << "-----" << std::endl;

    // ����� ������������� ��� K-C ��������
    output << "D_ = " << D_ << std::endl;
    output << "TABLE_D = " << tableD_ << std::endl;

    output << "FB: " << std::endl;
    for (int i = 0; i < k; i++)
        output << FB[i] << std::endl;
    output << "-----" << std::endl;

    output << "FE: " << std::endl;
    for (int i = 0; i < k; i++)
        output << FE[i] << std::endl;
    output << "-----" << std::endl;


    // ���� ��������� ������ �� �����
    for (int number = 0; number < length; number++)
        output << std::setprecision(6) << data[number].stress << std::endl;

    output.close();
};
double getPDFValue(double z)
{
	return exp(-z * z / 2) / sqrt(2 * M_PI);
};
void findMinMaxElements()
{
    minIndex = 0;
    maxIndex = 0;
    for (int i = 0; i < length; i++)
    {
        if (data[minIndex].stress > data[i].stress)
            minIndex = i;
        if (data[maxIndex].stress < data[i].stress)
            maxIndex = i;
    };
};
bool checkHiCoeff()
{
	
	double min = data[minIndex].stress; // ����������� �������� �������
	double max = data[maxIndex].stress; // ������������ �������� �������

	// ��������� ������ �������
	step = (data[maxIndex].stress - data[minIndex].stress) / k;

	// ��������� ������� ����� ��������
	border[0] = data[minIndex].stress;
	for (int i = 1; i < k + 1; i++)
		border[i] = border[i - 1] + step;
	border[k] = data[maxIndex].stress;

	// ��������� �������� ������� x
	for (int i = 0; i < k; i++)
		xMean[i] = min + i * step + step / 2;

	// ��������� �������� ������ ��� ���� ������� B
	for (int i = 0; i < length; i++)
		for (int j = 0; j < k; j++)
			if ((data[i].stress >= border[j]) && (data[i].stress <= border[j + 1]))
			{
				B[j]++;
				break;
			};

	// ��������� Bx � Bx^2, x � ������ � S � ������
	for (int i = 0; i < k; i++)
		BX[i]  = B[i] * xMean[i];
	for (int i = 0; i < k; i++)
		BX2[i] = B[i] * xMean[i] * xMean[i];
	for (int i = 0; i < k; i++)
		sumBX  += BX[i];
	for (int i = 0; i < k; i++)
		sumBX2 += BX2[i];

	x_ = sumBX / length;
	S_ = sqrt((sumBX2 - sumBX * sumBX / length) / (length - 1));

	// ��������� k'
	k_ = length * step / S_;

	// ����� ������ z
	for (int i = 0; i < k; i++)
		z[i] = (xMean[i] - x_) / S_;

	// ����� ������ F(z), E, B - E, (B - E)^2, HI
	for (int i = 0; i < k; i++)
		FZ[i]  = getPDFValue(z[i]);
	for (int i = 0; i < k; i++)
		E[i]   = FZ[i] * k_;
	for (int i = 0; i < k; i++)
		BE[i]  = B[i] - E[i];
	for (int i = 0; i < k; i++)
		BE2[i] = BE[i] * BE[i];
    HI = 0;
	for (int i = 0; i < k; i++)
		HI    += BE2[i] / E[i];

	// �������� �������� ������������ �������������
	if (HI > tableHI)
	{
		int count = 0; for (int i = 0; i < k; i++) count += B[i];
		std::cout << "B = " << count << std::endl;

		std::cout << "arythMean = " << arythMean << std::endl;
		std::cout << "step = " << step << std::endl;
		std::cout << "S_   = " << S_ << std::endl;
		std::cout << "K'   = " << k_ << std::endl;
		std::cout << "HI^2 = " << HI << std::endl;
		return false;
	};

	// ��������� F_B, F_E, F_B - F_E
	FB[0] = B[0];
	FE[0] = E[0];
	for (int i = 1; i < k; i++)
	{
		FB[i]   = FB[i - 1] + B[i];
		FE[i]   = FE[i - 1] + E[i];
		FBFE[i] = std::abs(FB[i] - FE[i]);
	};

	// ������� ������������ �������� |F_B - F_E|, ��������� �-� ��������
	int tempMax = 0;
	for (int i = 0; i < k; i++)
		if (FBFE[tempMax] < FBFE[i])
			tempMax = i;
	D_ = FBFE[tempMax] / length;

	// �������� �������� ������������ �������������
	if (D_ > tableD_)
		return false;

	return true;
};

int main()
{
    inputData(inputFile);
    bool flag = true;

    getSampleCoeff();

    while (flag)
    {
	    // ���������� ���������� �������������
        getSampleCoeff();

	    // ����� ������ ������������
        flag = screenErrors();
    };

	// �������������� �������� ������
	findMinMaxElements();
	double median = (data[minIndex].stress + data[maxIndex].stress) / 2;
    for (int i = 0; i < length; i++)
        ;//data[i].stress = log10(data[i].stress);

	getSampleCoeff();

    flag = false;
    if (covariance <= 0.33)
        flag = checkNormalDist();

	k = (int)floor(1 + 3.32 * log10(length));
	findMinMaxElements();
	flag = checkHiCoeff();

	std::cout << "length = " << length << std::endl;
    std::cout << "k = " << k << std::endl;
    std::cout << "Hi^2 = " << HI << std::endl;
    std::cout << "Flag = " << flag << std::endl;
    outputData(outputFile);

	return 0;
};