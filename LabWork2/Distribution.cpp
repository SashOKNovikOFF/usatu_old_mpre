#include "Distribution.h"

#include <ctime> // for time()
#include <fstream> // for fstream class
#include <iomanip> // for setprecision()

Distribution::Distribution()
{
	lengthSequence = 1000;
	numbers = 100;
	leftBorder = 0;
	rightBorder = 100;
	results.resize(lengthSequence);
	difference.resize(lengthSequence);
};

Distribution::Distribution(int length_, int numbers_, int leftB_, int rightB_)
{
	lengthSequence = length_;
	numbers = numbers_;
	leftBorder = leftB_;
	rightBorder = rightB_;
	results.resize(length_);
	difference.resize(length_);
};

Distribution::~Distribution()
{

};

void Distribution::getDistribution(bool offsetFlag, double offset)
{
	results.clear();
	results.resize(lengthSequence);
	difference.clear();
	difference.resize(lengthSequence);

	// �������
	double median = (leftBorder + rightBorder) / 2.0;

	// ��������� ������� ��������� �����
	for (int i = 0; i < numbers; i++)
		for (int j = 0; j < lengthSequence; j++)
			results[j] += rand() % (rightBorder - leftBorder) + leftBorder;
	for (int i = 0; i < lengthSequence; i++)
	{
		results[i] /= numbers;
		difference[i] = median - results[i];
	};

	// �������� ����������� �������������
	if (offsetFlag)
	{
		for (int i = 0; i < lengthSequence; i++)
		if ((difference[i] < 15.0) && ((i % 10) != 0))
		{
			results[i] += offset;
			difference[i] = median - results[i];
		};
	};
};

void Distribution::getOutputFile(std::string outputFile)
{
	// ������� ��������
	double x = 0.0;
	for (int i = 0; i < lengthSequence; i++)
		x += results[i];
	x /= lengthSequence;

	// ����� ��������� ��������� �������� �������� � �������� ��������
	double deviation_1 = 0.0;
	for (int i = 0; i < lengthSequence; i++)
		deviation_1 += pow(results[i] - x, 2);

	// ����������� ����������
	double deviation_2 = sqrt(deviation_1 / (lengthSequence - 1));

	// ������������������ ����������
	deviation_1 = sqrt(deviation_1 / lengthSequence);

	// ���������
	double variance = pow(deviation_1, 2);

	// ����� � ����
	std::fstream data(outputFile, std::ifstream::out);
	data << std::fixed << std::setprecision(6);
	data << "������� = " << getMedian() << std::endl;
	data << "������� �������������� ������� = " << x << std::endl;
	data << "������������������ ���������� = " << deviation_1 << std::endl;
	data << "����������� ���������� = " << deviation_2 << std::endl;
	data << "��������� = " << variance << std::endl;

	data << "delta_x_i: " << std::endl;
	data << std::setprecision(2);
	for (int i = 0; i < lengthSequence; i++)
		data << difference[i] << std::endl;

	data.close();
};

double Distribution::getMedian()
{
	return (leftBorder + rightBorder) / 2.0;
};

std::vector<double> Distribution::getResults()
{
	return results;
};

std::vector<double> Distribution::getDifference()
{
	return difference;
};

void Distribution::setResults(std::vector<double> results_)
{
	results = results_;
};

void Distribution::setDifference(std::vector<double> difference_)
{
	difference = difference_;
};