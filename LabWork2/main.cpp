#include "Distribution.h"

#include <cmath>
#include <fstream>
#include <string>
#include <iostream> // for cout()
#include <iomanip>  // for setprecision()
#include <ctime>    // for time()
#include <vector>

int main(int argc, char* argv[])
{
	srand((unsigned int)time(NULL));
	std::string fileName = "Input.txt";

	Distribution distObject;

	// 1-�� �������

	Distribution distObject2(1000, 2, 0, 100);
	Distribution distObject5(1000, 5, 0, 100);
	Distribution distObject10(1000, 10, 0, 100);

	distObject2.getDistribution(false, 0.0);
	distObject2.getOutputFile("Distribution 2.txt");

	distObject5.getDistribution(false, 0.0);
	distObject5.getOutputFile("Distribution 5.txt");

	distObject10.getDistribution(false, 0.0);
	distObject10.getOutputFile("Distribution 10.txt");

	// 2-�� �������
	
	distObject.getDistribution(false, 0.0);
	std::vector<double> results_C = distObject.getResults();
	distObject.getOutputFile("Input C.txt");

	distObject.getDistribution(true, 5.0);
	std::vector<double> results_L = distObject.getResults();
	distObject.getOutputFile("Input L.txt");

	distObject.getDistribution(true, -10.0);
	std::vector<double> results_R = distObject.getResults();
	distObject.getOutputFile("Input R.txt");

	int lengthSequence = (int)results_C.size();

	std::vector<double> results(lengthSequence);
	std::vector<double> difference(lengthSequence);
	double median = distObject.getMedian();

	for (int i = 0; i < lengthSequence; i++)
	{
		results[i] = (results_C[i] + results_L[i] + results_R[i]) / 3.0;
		difference[i] = median - results[i];
	};
	
	distObject.setResults(results);
	distObject.setDifference(difference);
	distObject.getOutputFile("Input.txt");

	return 0;
};