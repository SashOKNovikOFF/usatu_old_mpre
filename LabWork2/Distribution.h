#pragma once

#include <vector>
#include <string>

class Distribution
{
private:
	int lengthSequence;
	int numbers;
	int leftBorder;
	int rightBorder;
	std::vector<double> results;
	std::vector<double> difference;
public:
	Distribution();
	Distribution(int length_, int numbers_, int leftB_, int rightB_);
	~Distribution();

	void getDistribution(bool offsetFlag, double offset);
	void getOutputFile(std::string outputFile);
	double getMedian();

	std::vector<double> getResults();
	std::vector<double> getDifference();
	void setResults(std::vector<double> results_);
	void setDifference(std::vector<double> difference_);
};