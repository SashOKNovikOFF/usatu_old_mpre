#include <cmath>
#include <fstream>
#include <string>
#include <ctime> // for time()
#include <vector>

int main(int argc, char* argv[])
{
	std::string fileName = "Input.txt";

	// ������� ��������� ������
	int numbers = 100;
	int intervals = 10;
	int leftBorder = 10 * 10;
	int rightBorder = 1994;
	double step = ((rightBorder - leftBorder) / double(intervals));
	std::vector<int> results(numbers);
	std::vector<int> histogram(intervals, 0);

	// ��������� ������� ��������� �����
	srand((unsigned int)time(NULL));
	for (int i = 0; i < numbers; i++)
	{
		results[i] = rand() % (rightBorder - 1) + (leftBorder + 1);
		histogram[(int)((results[i] - leftBorder) / step)]++;
	};

	// ������� �������������� �������
	double x = 0.0;
	for (int i = 0; i < numbers; i++)
		x += results[i];
	x /= numbers;

	// ������������������ ����������
	double deviation_1 = 0.0;
	for (int i = 0; i < numbers; i++)
		deviation_1 += pow(results[i] - x, 2);
	deviation_1 = sqrt(deviation_1 / numbers);

	// ����������� ����������
	double deviation_2 = sqrt(numbers / (numbers - 1) * deviation_1);

	// ���������
	double variance = 0.0;
	for (int i = 0; i < numbers; i++)
		variance += pow(results[i], 2) - pow(x, 2);

	// ����� � ����
	std::fstream data(fileName, std::ifstream::out);
	data << "������� �������������� ������� = " << x << std::endl;
	data << "������������������ ���������� = " << deviation_1 << std::endl;
	data << "����������� ���������� = " << deviation_2 << std::endl;
	data << "��������� = " << variance << std::endl;
	
	data << "x_i: ";
	for (int i = 0; i < numbers; i++)
		data << results[i] << "\t";
	data << std::endl;

	data << "����� ���������\t���������� �����" << std::endl;
	for (int i = 0; i < histogram.size(); i++)
		data << i + 1 << "\t" << histogram[i] << std::endl;

	data.close();

	return 0;
};