set terminal png size 1280, 1024
set output 'Graph.png'

#set terminal wxt persist

min = 0.0
max = 100.0
border = (max + min) / 2.0

intervals = 50.0
width = (max - min) / intervals
temp = width * intervals / 10.0

set xrange [-20:20]
set yrange [0:400]
#set xtics min,temp,max
#set xtics min,width,max

set xlabel "Axis OX (Random numbers)"
set ylabel "Axis OY (Number of hits)"

bin(x) = width * (floor((x - (-border)) / width) + 0.5) + (-border)
set boxwidth width

plot 'Input L.txt' using (bin($1)):(1.0) smooth frequency with boxes fs solid 0.5 ti "Histogram L", \
     'Input R.txt' using (bin($1)):(1.0) smooth frequency with boxes fs solid 0.5 ti "Histogram R", \
     'Input C.txt' using (bin($1)):(1.0) smooth frequency with boxes fs solid 0.5 ti "Histogram C", \
     'Input.txt'   using (bin($1)):(1.0) smooth frequency with boxes fs solid 0.5 ti "Histogram"
	 