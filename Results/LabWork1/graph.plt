set terminal png size 1280, 1024
set output '10 (3).png'

#set terminal wxt persist

min = 100.0
max = 1994.0
intervals = 3.0
width = (max - min) / intervals
temp = width * intervals / 10.0

set xrange [min:max]
set yrange [0:6]
#set xtics min,temp,max
set xtics min,width,max

set xlabel "Axis OX (Random numbers)"
set ylabel "Axis OY (Number of hits)"

bin(x) = width * (floor((x - min) / width) + 0.5) + min
set boxwidth width

plot 'Input 10.txt' using (bin($1)):(1.0) smooth frequency with boxes fs solid 0.5 ti "Histogram"