set terminal wxt persist

### For .jpeg file creation
set terminal jpeg size 640, 480
set output "Graphic.jpg"

plot "SourceData_1.txt" using ($1):($2) notitle, \
     6.19732 + 14.7485 * x title 'y = 6.19732 + 14.7485 * x'