#define _SCL_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES

#include "Data.h"

#include <fstream>
#include <vector>
#include <iostream>
#include <iomanip>

int length = 500;                      // ����� ��������� �������
std::vector<Data> data(length);         // ��������� ������
std::vector<double> medianDiff(length); // ���������� ������� �� �������

std::string inputFile = "SourceData_1.txt"; // ������� ����
std::string outputFile = "Result.txt";        // �������� ����

int main()
{
    // �������� ������ ��� ����� ������ �� �����
    std::ifstream file(inputFile);

    // ���� ��������� ������ �� �����
    double temp;
    for (int number = 0; number < length; number++)
    {
        file >> temp;
        data[number].ping = temp;

        file >> temp;
        data[number].stress = temp;
    };

    file.close();

    // �������� ������ ��� ����� ������ �� �����
    std::ofstream output(outputFile);

	// 1. --------------------

	double SUM_X = 0;
	double SUM_Y = 0;
	double SUM_X_2 = 0;
	double SUM_Y_2 = 0;
	double SUM_XY = 0;
	double ARYTH_X = 0;
	double ARYTH_Y = 0;

	// ����� ���� ���������� x
	for (int i = 0; i < length; i++)
		SUM_X += data[i].ping;

	// ����� ���� �������� y
	for (int i = 0; i < length; i++)
		SUM_Y += data[i].stress;

	// ����� ��������� ���������� x
	for (int i = 0; i < length; i++)
		SUM_X_2 += data[i].ping * data[i].ping;

	// ����� ��������� �������� y
	for (int i = 0; i < length; i++)
		SUM_Y_2 += data[i].stress * data[i].stress;

	// ����� ������������ x * y
	for (int i = 0; i < length; i++)
		SUM_XY += data[i].ping * data[i].stress;

	// ������� �������� x
	ARYTH_X = SUM_X / length;

	// ������� �������� y
	ARYTH_Y = SUM_Y / length;

	output << "SUM_X = " << SUM_X << std::endl;
	output << "SUM_Y  = " << SUM_Y << std::endl;
	output << "SUM_X_2 = " << SUM_X_2 << std::endl;
	output << "SUM_Y_2 = " << SUM_Y_2 << std::endl;
	output << "SUM_XY = " << SUM_XY << std::endl;
	output << "ARYTH_X = " << ARYTH_X << std::endl;
	output << "ARYTH_Y = " << ARYTH_Y << std::endl;

	// 2. --------------------

	double Q_X, Q_Y, Q_XY;

	// ���������� ������������� �������
	Q_X = SUM_X_2 - SUM_X * SUM_X / length;
	Q_Y = SUM_Y_2 - SUM_Y * SUM_Y / length;
	Q_XY = SUM_XY - SUM_X * SUM_Y / length;
	
	output << "Q_X = " << Q_X << std::endl;
	output << "Q_Y  = " << Q_Y << std::endl;
	output << "Q_XY = " << Q_XY << std::endl;

	// 3. --------------------

	double b0, b1;

	// ���������� ������������� �������� ���������
	b1 = Q_XY / Q_X;
	b0 = ARYTH_Y - b1 * ARYTH_X;
	
	output << "b0 = " << b0 << std::endl;
	output << "b1 = " << b1 << std::endl;

	// 4. --------------------

	double S_ = 0;

	// ���������� ��������� ��������� �������� �����
	for (int i = 0; i < length; i++)
		S_ += (data[i].stress - ARYTH_Y) * (data[i].stress - ARYTH_Y);
	S_ = S_ / (length - 2);
	
	output << "S_ = " << S_ << std::endl;

	// 5. --------------------

	double Q_YX;

	// ���������� ������������� ��������
	Q_YX = Q_Y - b1 * Q_XY;
	
	output << "Q_YX = " << Q_YX << std::endl;

	// 6. --------------------

	double R;
	double R_TABLE = 0.0875;

	// ���������� ������������ ����������
	R = Q_XY / sqrt(Q_X * Q_Y);
	
	output << "R = " << R << std::endl;
	output << "R_TABLE = " << R_TABLE << std::endl;

	output.close();

	return 0;
};